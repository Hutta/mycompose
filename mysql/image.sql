-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 13, 2016 at 10:24 PM
-- Server version: 10.1.8-MariaDB
-- PHP Version: 5.6.14

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `image` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `image`;


SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `image`
--

-- --------------------------------------------------------

--
-- Table structure for table `img`
--

CREATE TABLE `img` (
  `ID` int(11) NOT NULL,
  `url` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `img`
--

INSERT INTO `img` (`ID`, `url`) VALUES
(1, '/assets/img/1.jpg'),
(3, '/assets/img/2.jpg'),
(4, '/assets/img/3.jpg'),
(5, '/assets/img/4.jpg'),
(6, '/assets/img/5.jpg'),
(7, '/assets/img/6.jpg'),
(8, '/assets/img/7.jpg'),
(9, '/assets/img/8.jpg'),
(10, '/assets/img/9.jpg'),
(11, '/assets/img/10.jpg'),
(12, '/assets/img/11.jpg'),
(13, '/assets/img/12.jpg'),
(14, '/assets/img/13.jpg'),
(15, '/assets/img/14.jpg'),
(16, '/assets/img/15.jpg'),
(17, '/assets/img/16.jpg'),
(18, '/assets/img/17.jpg'),
(19, '/assets/img/18.jpg'),
(20, '/assets/img/19.jpg'),
(21, '/assets/img/20.jpg'),
(22, '/assets/img/21.jpg'),
(23, '/assets/img/22.jpg'),
(24, '/assets/img/23.jpg'),
(25, '/assets/img/24.jpg'),
(26, '/assets/img/25.jpg'),
(27, '/assets/img/26.jpg'),
(28, '/assets/img/27.jpg'),
(29, '/assets/img/28.jpg'),
(30, '/assets/img/29.jpg'),
(31, '/assets/img/30.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `img`
--
ALTER TABLE `img`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `img`
--
ALTER TABLE `img`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
