<?php

    $mysqli = new mysqli('mysql', 'root', 'password', 'image');
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $countries = $mysqli->query("SELECT url FROM img ORDER BY RAND() LIMIT 20");

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>My Pictures</title>
    <meta charset="utf-8">
    <!-- <link rel="stylesheet" href="/assets/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">

</head>
<body>

    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>My Pictures</h2>
                <br/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="text-center" >
                    <!-- <img src="/assets/img/world.jpg" class="img-thumbnail"> -->
                    <div>
                   
                      
                        <hr>
                    </div>
                </div>
                <table class="table table-striped">
                    
                    <tbody>

                        <?php
                        $i = 0;
                        $j = 0;
                        while($row = $countries->fetch_assoc()) {
                            if($i%5 == 0){
                             echo '<tr>';
                             $j = 0;
                            }
                
                            echo '<td><img src="'.strtolower($row["url"]).'" style="width: 100px;height: 100px;" /></td>';
                            $j++;

                            if($j%5 == 0)
                             echo '</tr>';

                            $i++;
                        } 
                        ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

</body>
</html>